#![allow(non_snake_case)]

use anyhow::Result;
use serde_derive::{Deserialize, Serialize};
use serde_json::json;
use std::os::unix::fs;

use base64::encode;
use std::collections::HashMap;
use std::fs::{create_dir, remove_dir_all, File};
use std::io::{Read, Write};
use std::path::Path;
use std::process::{Command, Stdio};
use std::str;
use std::vec::Vec;

use bollard::container::{InspectContainerOptions, ListContainersOptions};
use bollard::Docker;
use tokio::runtime::Runtime;

use regex::Regex;

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct ExtraMount {
    container_path: String,
    host_path: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct PortMapping {
    container_port: u32,
    host_port: u32,
    protocol: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct Node {
    role: String,
    extra_mounts: Vec<ExtraMount>,
    extra_port_mappings: Vec<PortMapping>,
    kubeadm_config_patches: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct ClusterConfig {
    kind: String,
    api_version: String,
    nodes: Vec<Node>,
    containerd_config_patches: Vec<String>,
}

#[derive(Deserialize, Debug)]
struct DockerLogin {
    #[serde(alias = "Username")]
    username: String,
    #[serde(alias = "Secret")]
    secret: String,
}

pub struct Kind {
    pub name: String,
    pub ecr_repo: Option<String>,
    config_dir: String,
    local_registry_port: Option<String>,
    local_registry_name: Option<String>,
    extra_port_mapping: Option<String>,
    verbose: bool,
}

impl Kind {
    fn extra_mount(container_path: Option<&str>, host_path: Option<&str>) -> Vec<ExtraMount> {
        if let Some(container_path) = container_path {
            if let Some(host_path) = host_path {
                return vec![ExtraMount {
                    container_path: String::from(container_path),
                    host_path: String::from(host_path),
                }];
            }
        }

        vec![]
    }

    fn kind_node(role: &str, container_path: Option<&str>, host_path: Option<&str>) -> Node {
        Node {
            role: String::from(role),
            extra_mounts: Kind::extra_mount(container_path, host_path),
            extra_port_mappings: vec![],
            kubeadm_config_patches: vec![],
        }
    }

    fn init_config_ingress_ready() -> String {
        String::from(
            r#"kind: InitConfiguration
nodeRegistration:
  kubeletExtraArgs:
    node-labels: "ingress-ready=true""#,
        )
    }

    fn get_kind_cluster_config(
        &self,
    ) -> ClusterConfig {
        let mut cc = ClusterConfig {
            kind: String::from("Cluster"),
            api_version: String::from("kind.x-k8s.io/v1alpha4"),
            nodes: vec![],
            containerd_config_patches: vec![],
        };

        if let Some(ecr) = &self.ecr_repo {
            if let Ok(docker_path) = self.create_docker_ecr_config_file(ecr) {
                cc.nodes = vec![Kind::kind_node(
                    "control-plane",
                    Some("/var/lib/kubelet/config.json"),
                    Some(&docker_path),
                )];
            }
        }

        if let Some(local_registry_port) = &self.local_registry_port {
            if let Some(local_registry_name) = &self.local_registry_name {
            cc.containerd_config_patches =
                vec![Kind::get_containerd_config_patch_to_local_registry(
                    &local_registry_name, &local_registry_port,
                )];
            }
        }

        cc
    }

    // Returns a "patch" formatted the way containerd expects it, allowing it to
    // find and proxy requests to a Container images registry running in localhost.
    fn get_containerd_config_patch_to_local_registry(registry_name: &str, port: &str) -> String {
        format!(
            r#"
[plugins."io.containerd.grpc.v1.cri".registry.mirrors."localhost:{}"]
  endpoint = ["http://{}:{}"]"#,
            port.trim(),
            registry_name,
            port.trim(),
        )
    }

    /// Gets the Kind cluster name from the Docker container name.
    fn get_cluster_name(container_name: &str) -> Option<String> {
        if !container_name.ends_with("-control-plane") {
            None
        } else {
            Some(
                container_name
                    .replace("-control-plane", "")
                    .replace("/", ""),
            )
        }
    }

    // Removes every entry in ~/.hake that does not have a corresponding kind docker container.
    async fn async_get_containers() -> Result<Vec<String>> {
        let docker = Docker::connect_with_local_defaults()?;
        let mut filter = HashMap::new();
        filter.insert(String::from("status"), vec![String::from("running")]);
        let containers = &docker
            .list_containers(Some(ListContainersOptions {
                all: true,
                filters: filter,
                ..Default::default()
            }))
            .await?;

        let mut kind_containers = Vec::new();
        for container in containers {
            if let Some(image) = &container.image {
                if image.starts_with("kindest/node") {
                    if let Some(name) = &container.names {
                        let n = name.get(0).unwrap();
                        match Kind::get_cluster_name(&n) {
                            Some(n) => kind_containers.push(n),
                            None => continue,
                        }
                    }
                }
            }
        }

        Ok(kind_containers)
    }

    // Finds the Port in which the container is presenting the Container registry in
    // localhost.
    async fn inspect_container_network(container_name: &str) -> Result<String> {
        let docker = Docker::connect_with_local_defaults()?;
        let container_options = Some(InspectContainerOptions{size: false});

        let inspect = docker.inspect_container(container_name, container_options).await?;

        let network_settings = inspect.network_settings.expect("Could not inspect network");

        // NOTE: I'm not sure why, but I used to configure the registry with the IP it is listening
        // on localhost. Now I realize that the patch points at the "container name" and the Port.
        // So whatever IP this registry is listening on, it is not relevant. Maybe it goes through
        // Docker networking, instead of jumping to localhost?
        // TODO: Can we do this in a more rusty way?
        // let ip_address = network_settings.
        //     ip_address.
        //     expect("Could not read IP from network").
        //     clone();

        let ports = network_settings.
            ports.
            expect("Could not read Ports from network");

        if ports.len() > 1 {
            println!("{} has more than 1 Ports section, using first one", &container_name);
        }

        // TODO: Ports might `is_empty()` we should return error in that case

        // TODO: We'll use the first Port allocated to this container
        let mut port_keys = ports.keys();
        let first_key = port_keys.next().unwrap();

        let mut host_port = String::new();
        if let Some(first_value) = ports.get(first_key) {
            first_value.as_ref().unwrap().into_iter().for_each(|port_binding| {
                host_port = port_binding.host_port.clone().unwrap();
            });
        };

        // let result = format!("{}:{}", ip_address, host_port);
        // dbg!(&result);
        Ok(host_port)
    }

    pub fn get_kind_containers() -> Result<Vec<String>> {
        Runtime::new().unwrap().block_on(Kind::async_get_containers())
    }

    pub fn inspect_container(registry: &str) -> Result<String> {
        Runtime::new().unwrap().block_on(Kind::inspect_container_network(registry))
    }


    fn get_docker_login(registry: &str) -> Result<String> {
        let creds = Kind::get_docker_credentials_from_helper(registry)?;

        let login: DockerLogin = serde_json::from_str(&creds)?;
        let encoded = encode(&format!("{}:{}", login.username, login.secret));

        Ok(json!({
                "auths": {
                    registry: {
                        "auth": encoded
                    }
                }
            }
        )
        .to_string())
    }

    fn get_docker_credentials_from_helper(registry: &str) -> Result<String> {
        let mut cmd = Command::new("docker-credential-ecr-login")
            .arg("get")
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()
            .unwrap_or_else(|_| {
                panic!("Could not find docker credentials helper for {}", registry)
            });

        cmd.stdin.as_mut().unwrap().write_all(registry.as_bytes())?;
        cmd.wait()?;

        let mut output = String::new();
        cmd.stdout.unwrap().read_to_string(&mut output)?;

        Ok(output)
    }

    fn create_docker_ecr_config_file(&self, ecr: &str) -> Result<String> {
        let docker_login = Kind::get_docker_login(ecr).expect("could not get docker login");

        // save docker_login()
        let docker_config_path = format!("{}/docker_config", self.config_dir);
        let mut docker_config = File::create(&docker_config_path)?;
        docker_config.write_all(&docker_login.into_bytes())?;

        Ok(docker_config_path)
    }

    fn create_dirs(cluster_name: &str) -> Result<()> {
        let home = Kind::get_config_dir();

        if !Path::new(&home).exists() {
            create_dir(&home)?;
        }
        let new_dir = format!("{}/{}", &home, cluster_name);
        create_dir(new_dir)?;

        Ok(())
    }

    // Returns the hake configuration directory.
    pub fn get_config_dir() -> String {
        crate::get_config_dir()
    }

    pub fn configure_private_registry(&mut self, reg: Option<String>) {
        self.ecr_repo = reg;
    }

    pub fn set_verbose(&mut self, verbose: bool) {
        self.verbose = verbose;
    }

    pub fn use_local_registry(&mut self, container_name: &str) -> Result<()>{
        let local_registry = Kind::inspect_container(container_name).
            expect("Registry in container was not found");
        self.local_registry_port = Some(local_registry);
        self.local_registry_name = Some(container_name.to_string());

        Ok(())
    }

    pub fn extra_port_mapping(&mut self, extra_port_mapping: &str) {
        self.extra_port_mapping = Some(String::from(extra_port_mapping));
    }

    /// receives a string like: 80:80:TCP or 80:80 or 80
    fn parse_extra_port_mappings(epm: &str) -> Option<PortMapping> {
        let mut container_port = 0;
        let mut host_port = 0;
        let mut proto = String::from("TCP");

        let re0 = Regex::new(r"^(\d{2}):(\d{2}):(TCP|HTTP)$").unwrap();
        let re1 = Regex::new(r"^(\d{2}):(\d{2})$").unwrap();
        let re2 = Regex::new(r"^(\d+)$").unwrap();

        if re0.is_match(epm) {
            let cap = re0.captures(epm).unwrap();
            container_port = cap[1].parse::<u32>().unwrap();
            host_port = cap[2].parse::<u32>().unwrap();
            proto = String::from(&cap[3]);
        } else if let Some(cap) = re1.captures(epm) {
            container_port = cap[1].parse::<u32>().unwrap();
            host_port = cap[2].parse::<u32>().unwrap();
        } else if let Some(cap) = re2.captures(epm) {
            container_port = cap[1].parse::<u32>().unwrap();
            host_port = container_port;
        }

        if container_port != 0 {
            Some(PortMapping {
                container_port,
                host_port,
                protocol: proto,
            })
        } else {
            None
        }
    }

    pub fn create(self) -> Result<()> {
        Kind::create_dirs(&self.name)?;

        let mut args = vec!["create", "cluster"];
        let kubeconfig;

        args.push("--name");
        args.push(&self.name);

        kubeconfig = format!("{}/kubeconfig", self.config_dir);
        args.push("--kubeconfig");
        args.push(&kubeconfig);

        args.push("--config");
        let mut kind_config = self.get_kind_cluster_config();
        if let Some(extra_port_mapping) = self.extra_port_mapping {
            let epm = Kind::parse_extra_port_mappings(&extra_port_mapping);
            if let Some(epm) = epm {
                if let Some(mut node) = kind_config.nodes.get_mut(0) {
                    node.extra_port_mappings = vec![epm];
                } else {
                    let mut nn = Kind::kind_node("control-plane", None, None);
                    nn.extra_port_mappings = vec![epm];
                    kind_config.nodes = vec![nn];
                };
                kind_config.nodes[0].kubeadm_config_patches =
                    vec![Kind::init_config_ingress_ready()];
            }
        }

        // dbg!(&kind_config);
        let kind_cluster_config = serde_yaml::to_string(&kind_config)?;

        let kind_config_path = format!("{}/kind_config", self.config_dir);
        let mut kind_config = File::create(&kind_config_path)?;
        kind_config.write_all(&kind_cluster_config.into_bytes())?;

        // point the config file to the one we just saved
        args.push(&kind_config_path);
        Kind::run(&args, self.verbose)?;

        // Creates a symbolic link in config directory
        let kubeconfig_symbolic_link = format!("{}/{}.config", Kind::get_config_dir(), &self.name);
        fs::symlink(&kubeconfig, kubeconfig_symbolic_link)?;

        let kind_args_file = format!("{}/{}/kind_args", Kind::get_config_dir(), &self.name);
        let mut saved_args = File::create(kind_args_file)?;
        saved_args.write_all(args.join(" ").as_bytes())?;

        Ok(())
    }

    pub fn run(args: &[&str], verbose: bool) -> Result<()> {
        let mut command = Command::new("kind");
        command.args(args);
        if verbose {
            command.spawn()?.wait()?;
        } else {
            command.output()?;
        }

        Ok(())
    }

    pub fn recreate(name: &str, verbose: bool) -> Result<()> {
        let config_dir = format!("{}/{}", Kind::get_config_dir(), name);
        let args_file = format!("{}/kind_args", config_dir);

        let mut contents = String::new();
        let mut saved_args = File::open(args_file)?;
        saved_args.read_to_string(&mut contents)?;

        Kind::delete_cluster(name)?;

        let args: Vec<&str> = contents.split_ascii_whitespace().collect();
        Kind::run(&args, verbose)?;

        Ok(())
    }

    pub fn delete(&self) -> Result<()> {
        Kind::delete_cluster(&self.name)?;

        remove_dir_all(&self.config_dir)?;

        Ok(())
    }

    fn delete_cluster(name: &str) -> Result<()> {
        let mut args = vec!["delete", "cluster"];
        args.push("--name");
        args.push(name);

        let _cmd = Command::new("kind").args(args).output()?;

        Ok(())
    }

    pub fn new(name: &str) -> Kind {
        let home = Kind::get_config_dir();

        Kind {
            name: String::from(name),
            ecr_repo: None,
            config_dir: format!("{}/{}", home, name),
            local_registry_name: None,
            local_registry_port: None,
            extra_port_mapping: None,
            verbose: false,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::kind::Kind;

    #[test]
    fn test_new() {
        // TODO: test configuration on home directory.
        let k = Kind::new("test");

        let home = dirs::home_dir().unwrap();

        assert_eq!(k.name, "test");
        assert_eq!(k.ecr_repo, None);
        assert_eq!(
            k.config_dir,
            format!("{}/.hake/test", home.to_str().unwrap())
        );
        assert_eq!(k.local_registry_port, None);
    }

    #[test]
    fn test_get_cluster_name() {
        assert_eq!(Kind::get_cluster_name("not-us"), None);
        assert_eq!(
            Kind::get_cluster_name("this-is-us-control-plane"),
            Some(String::from("this-is-us"))
        );
        assert_eq!(
            Kind::get_cluster_name("/this-is-us-control-plane"),
            Some(String::from("this-is-us"))
        );
    }
}
